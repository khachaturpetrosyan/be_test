require('dotenv').config();

let app = require('./index');
let port =  process.env.PORT || 8372;

app.listen(port, function() {
    console.log('Express server listening on port ' + port);
});

