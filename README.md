> **Note**: This repo is no longer officially maintained as of Jan, 2023.
> Feel free to use it, fork it and patch it for your own needs.
## The node.js example app

[![CircleCI](https://img.shields.io/circleci/project/github/contentful/the-example-app.nodejs.svg)](https://circleci.com/gh/contentful/the-example-app.nodejs)

The node.js example app teaches the very basics of how to work with Contentful:

- consume content from the Contentful Delivery and Preview APIs
- model content
- edit content through the Contentful web app

The app demonstrates how decoupling content from its presentation enables greater flexibility and facilitates shipping higher quality software more quickly.

## Common setup

Clone the repo and install the dependencies.

```bash
git clone https://gitlab.com/khachaturpetrosyan/be_test
cd be_test
```

```bash
npm ci
```

## Steps for read-only access

To start the express server, run the following

```bash
npm start
```

Open [http://localhost:8372](http://localhost:8372) and take a look around.

## Swagger API docs

Open [http://localhost:8372/api-docs](http://localhost:8372/api-docs) and take a look around.
