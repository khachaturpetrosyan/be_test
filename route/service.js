const express = require('express');
const router = express.Router();

const controller = require('../controller/service');
const validator = require('../middleware/validation/lat_lng_validation');

router.get('/coordinates', validator.lat_lng, controller.get_service_location);
router.get('/coordinates/db',  controller.get_service_location_db);


module.exports = router;
