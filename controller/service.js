const db = require('../db')
const  haversineDistance  = require('../utils/haversineDistance')


module.exports = {
    get_service_location: async (req, res) => {
        try {
            const { latitude, longitude } = req.query;

            const serviceProviders = await db.models.service_provider.findAll();

            const sortedProviders = serviceProviders.sort((a, b) => {
                const aDistance = haversineDistance(latitude, longitude, a.latitude, a.longitude);
                const bDistance = haversineDistance(latitude, longitude, b.latitude, b.longitude);
                return aDistance - bDistance;
            });

            const providersWithDistance = sortedProviders.map(provider => ({
                ...provider.toJSON(),
                distance: haversineDistance(latitude, longitude, provider.latitude, provider.longitude),
            }));

            res.json(providersWithDistance);

        } catch (e) {
            res.status(500).json({message: 'Please try again later', error: e?.message})
        }
    },


    get_service_location_db: async (req, res) => {
        try {
            const { latitude, longitude } = req.query;

            const providers = await db.models.service_provider.findAll({
                attributes: [
                    'id',
                    'name',
                    'latitude',
                    'longitude',
                    [
                        db.sequelize.literal(`6371 * acos(cos(radians(${latitude})) * cos(radians(latitude)) * cos(radians(longitude) - radians(${longitude})) + sin(radians(${latitude})) * sin(radians(latitude)))`),
                        'distance'
                    ]
                ],
                order: [
                    [db.sequelize.literal('distance'), 'ASC']
                ]
            });

            res.json(providers);
        } catch (e) {
            console.log(e);
            res.status(500).json({message: 'Please try again later', error: e?.message})
        }
    },
}
