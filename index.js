const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const {swagger} = require('./swagger');

swagger(app)
app.use(cors());

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));


let ServiceRoute = require('./route/service')
app.use('/services', ServiceRoute);

module.exports = app;
