const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

exports.swagger = (app) => {
    const options = {
        definition: {
            openapi: "3.0.0",
            info: {
                title: "TEST SERVICE PROVIDER API",
                version: "0.1.0",
                description:
                    "CRUD API",
            },
            servers: [
                {
                    url: "http://127.0.0.1:8372",
                },

            ],
            components: {
                securitySchemes: {
                    basicAuth: { },
                }
            },
            security: [{
                basicAuth: []
            }]
        },
        apis: ['./swagger_api/*.js'],
    };

    const specs = swaggerJsdoc(options);

    app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs, { explorer: true }));


}
