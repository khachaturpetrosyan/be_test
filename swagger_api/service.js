/**
 * 	@swagger
 * /services/coordinates:
 *  get:
 *   summary: 'Take services in db and filtering'
 *   description: ''
 *   tags:
 *    - 'service'
 *   security:
 *    - basicAuth: []
 *   parameters:
 *   - name: 'latitude'
 *     in: 'query'
 *     description: 'latitude'
 *     required: true
 *   - name : 'longitude'
 *     in: 'query'
 *     description: 'longitude'
 *     required: true
 *   responses:
 *    '200':
 *      description: 'You take data. type: Array,'
 *      content:
 *       application/json:
 *        schema:
 *         type: "object"
 *        style: simple
 */

/**
 * 	@swagger
 * /services/coordinates/db:
 *  get:
 *   summary: 'Take filtering services'
 *   description: ''
 *   tags:
 *    - 'service'
 *   security:
 *    - basicAuth: []
 *   parameters:
 *   - name: 'latitude'
 *     in: 'query'
 *     description: 'latitude'
 *     required: true
 *   - name : 'longitude'
 *     in: 'query'
 *     description: 'longitude'
 *     required: true
 *   responses:
 *    '200':
 *      description: 'You take data. type: Array,'
 *      content:
 *       application/json:
 *        schema:
 *         type: "object"
 *        style: simple
 */
