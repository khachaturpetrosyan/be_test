const defaultOptions = require('../defaultOptions');

module.exports = (sequelize, DataTypes) => {

    return sequelize.define('service_provider', {

        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },

        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        latitude: {
            type: DataTypes.DECIMAL(10, 8),
            allowNull: false,
        },

        longitude: {
            type: DataTypes.DECIMAL(11, 8),
            allowNull: false,
        },

    }, Object.assign({}, defaultOptions));
};
