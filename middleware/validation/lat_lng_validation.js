let lat_lng = async (req, res, next) => {
    const { latitude, longitude } = req.query;
    console.log(req.query);
    const regexLat = /^(-?[1-8]?\d(?:\.\d{1,18})?|90(?:\.0{1,18})?)$/;
    const regexLon = /^(-?(?:1[0-7]|[1-9])?\d(?:\.\d{1,18})?|180(?:\.0{1,18})?)$/;

    if( !regexLat.test(latitude) || !regexLon.test(longitude) ) {
        return res.status(403).json({message: 'Invalid latitude and longitude values'})
    }
    next();
}

module.exports = {
    lat_lng,
};
